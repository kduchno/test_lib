#!groovy

import jenkins.model.Jenkins.*
import hudson.model.Hudson.*
import com.cloudbees.plugins.credentials.impl.*;
import com.cloudbees.plugins.credentials.*;
import com.cloudbees.plugins.credentials.domains.*;

def call(String username = "kduchno") {
    def creds = com.cloudbees.plugins.credentials.CredentialsProvider.lookupCredentials(
        com.cloudbees.plugins.credentials.common.StandardUsernamePasswordCredentials.class,
        jenkins.model.Jenkins.instance
    )

    def c = creds.findResult { it.username == username ? it : null }

    // return "thuderbird"
    return c.id
    // echo c.id

    // if ( c ) {
        // return c.id
    //     println "found credential ${c.id} for username ${c.username}"
    //
    //     def credentials_store = jenkins.model.Jenkins.instance.getExtensionList(
    //         'com.cloudbees.plugins.credentials.SystemCredentialsProvider'
    //         )[0].getStore()
    //
    //      println "result: " + credentials_store
    // } else {
    //   println "could not find credential for ${username}"
//    }

}

// eof
